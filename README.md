# Package warehouse

### Howto running

Application is compatible with java 11.

Create jar package with this command. After package find file in targer directory:
`mvn package`

Start application with this command:
`java -jar package-warehouse.jar`

Application support import from file. In this case run application with argument. Example:
`java -jar package-warehouse.jar /path/to/file`

### Input documentation

Manual input data sample: `3.4 08801`

Description: first number is weight in positive decimal number with maximal 3 decimal places, second is postcode (fix 5 length)

Input `quit` to application terminate.

Import file must be compatible with manual input.