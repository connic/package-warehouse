package cz.konir.packagewarehouse.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import cz.konir.packagewarehouse.database.Warehouse;
import cz.konir.packagewarehouse.service.impl.WarehouseServiceImpl;
import cz.konir.packagewarehouse.vo.Pack;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class WarehouseServiceTest {

    @Mock
    private Warehouse warehouse;

    @InjectMocks
    private WarehouseServiceImpl warehouseService;

    @Test
    void addPack() {

        Pack pack = new Pack("12345", 10.0);

        warehouseService.addPack(pack);

        verify(warehouse, times(1)).addPack(pack);
    }

    @Test
    void addPacks() {

        List<Pack> packs = new ArrayList<>();
        Pack pack1 = new Pack("12345", 10.0);
        Pack pack2 = new Pack("12346", 20.0);
        packs.add(pack1);
        packs.add(pack2);

        warehouseService.addPacks(packs);

        verify(warehouse, times(2)).addPack(any(Pack.class));
        verify(warehouse, times(1)).addPack(pack1);
        verify(warehouse, times(1)).addPack(pack2);
    }

    @Test
    void getWeightGroupedByPostCodes() {

        List<Pack> packs = new ArrayList<>();
        packs.add(new Pack("12345", 10.0));
        packs.add(new Pack("12346", 10.0));
        packs.add(new Pack("12346", 10.0));

        when(warehouse.getAllPack()).thenReturn(packs);

        Map<String, Double> groupedByPostCodes = warehouseService.getWeightGroupedByPostCodes();

        assertThat(groupedByPostCodes.size(), is(2));
        assertThat(groupedByPostCodes.get("12345"), is(10.0));
        assertThat(groupedByPostCodes.get("12346"), is(20.0));
    }
}