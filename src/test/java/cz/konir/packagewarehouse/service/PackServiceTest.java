package cz.konir.packagewarehouse.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import cz.konir.packagewarehouse.service.impl.PackServiceImpl;
import cz.konir.packagewarehouse.vo.Pack;
import java.util.List;
import org.junit.jupiter.api.Test;

class PackServiceTest {

    private PackService packService = new PackServiceImpl();

    @Test
    void shouldValidateStringsGood() {

        String[] strings = {"10.100", "12345"};
        boolean result = packService.validateStrings(strings);

        assertTrue(result);
    }

    @Test
    void shouldValidateStringsLongDecimalPosition() {

        String[] strings = {"10.1000", "12345"};
        boolean result = packService.validateStrings(strings);

        assertFalse(result);
    }

    @Test
    void shouldValidateStringsNegativeWeight() {

        String[] strings = {"-10.1000", "12345"};
        boolean result = packService.validateStrings(strings);

        assertFalse(result);
    }

    @Test
    void shouldValidateStringsWrongInput() {

        String[] strings = {"-10.100012345"};
        boolean result = packService.validateStrings(strings);

        assertFalse(result);
    }

    @Test
    void shouldValidateStringsWrongPostCode() {

        String[] strings = {"-10.100", "1234"};
        boolean result = packService.validateStrings(strings);

        assertFalse(result);
    }

    @Test
    void shouldValidateStringsWrongDecimalDelimiter() {

        String[] strings = {"-10,100", "1234"};
        boolean result = packService.validateStrings(strings);

        assertFalse(result);
    }

    @Test
    void createPack() {

        String[] strings = {"10.100", "12345"};
        Pack pack = packService.createPack(strings);

        assertThat(pack.getPostNumber(), is("12345"));
        assertThat(pack.getWeight(), is(10.1));
    }

    @Test
    void createPacksFromFileGood() {

        List<Pack> packsFromFile = packService.createPacksFromFile("src/test/resources/test-file-ok.txt");

        assertThat(packsFromFile.size(), is(5));
    }

    @Test
    void createPacksFromFileWrong() {

        List<Pack> packsFromFile = packService.createPacksFromFile("src/test/resources/test-file-wrong.txt");

        assertThat(packsFromFile.size(), is(0));
    }

    @Test
    void delimitString() {

        String[] strings = packService.delimitString("10.100 12345");
        assertThat(strings.length, is(2));
        assertThat(strings[0], is("10.100"));
        assertThat(strings[1], is("12345"));
    }
}