package cz.konir.packagewarehouse.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Pack {

    private String postNumber;
    private Double weight;

}
