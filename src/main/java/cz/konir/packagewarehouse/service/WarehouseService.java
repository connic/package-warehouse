package cz.konir.packagewarehouse.service;

import cz.konir.packagewarehouse.vo.Pack;
import java.util.List;
import java.util.Map;

public interface WarehouseService {

    void addPack(Pack pack);

    void addPacks(List<Pack> packs);

    Map<String, Double> getWeightGroupedByPostCodes();

}
