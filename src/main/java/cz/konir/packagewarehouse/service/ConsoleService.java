package cz.konir.packagewarehouse.service;

import cz.konir.packagewarehouse.vo.Pack;
import java.util.List;
import java.util.Scanner;
import javax.annotation.PreDestroy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConsoleService {

    private static final String ESCAPE_WORD = "quit";


    private final ConfigurableApplicationContext context;
    private final WarehouseService warehouseService;
    private final PackService packService;
    private final ApplicationArguments args;

    private final Scanner scanner = new Scanner(System.in);

    @EventListener(ApplicationReadyEvent.class)
    public void ConsoleReader() {

        System.out.println("Warehouse application started.");

        if (args.getSourceArgs().length > 0) {
            processCommandlineArgs(args.getSourceArgs());
        }

        boolean running = true;

        while (running) {

            String textLine = scanner.nextLine();

            if (!ObjectUtils.isEmpty(textLine)) {
                if (ESCAPE_WORD.equals(textLine)) {
                    running = false;
                    break;
                }

                String[] strings = packService.delimitString(textLine);

                if (packService.validateStrings(strings)) {

                    Pack pack = packService.createPack(strings);
                    warehouseService.addPack(pack);
                }
            }

        }

        SpringApplication.exit(context, () -> 0);

    }


    private void processCommandlineArgs(String[] args) {
        String filename = args[0];

        List<Pack> packs = packService.createPacksFromFile(filename);
        if (packs.size() > 0) {
            warehouseService.addPacks(packs);
            System.out.println(String.format("Imported %s packs.", packs.size()));
        }
    }


    @PreDestroy
    public void destroy() {
        scanner.close();
    }


}
