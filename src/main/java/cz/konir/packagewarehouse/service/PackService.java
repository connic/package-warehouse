package cz.konir.packagewarehouse.service;

import cz.konir.packagewarehouse.vo.Pack;
import java.util.List;

public interface PackService {

    boolean validateStrings(String[] strings);

    Pack createPack(String[] strings);

    List<Pack> createPacksFromFile(String path);

    String[] delimitString(String string);

}
