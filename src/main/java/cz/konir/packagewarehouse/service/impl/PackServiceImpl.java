package cz.konir.packagewarehouse.service.impl;

import cz.konir.packagewarehouse.service.PackService;
import cz.konir.packagewarehouse.vo.Pack;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Slf4j
@Service
@RequiredArgsConstructor
public class PackServiceImpl implements PackService {

    private static final int WORD_COUNT = 2;
    private static final int POST_CODE_LENGHT = 5;
    private static final String DOUBLE_PATTERN = "\\d+(\\.)?[0-9]{0,3}";
    private static final String DELIMITER = " ";

    @Override
    public boolean validateStrings(String[] strings) {
        if ((strings.length != WORD_COUNT) || (strings[1].length() != POST_CODE_LENGHT)) {
            errorWrite("Wrong input!");
            return false;
        }

        if (!Pattern.matches(DOUBLE_PATTERN, strings[0])) {
            errorWrite("Wrong input!");
            return false;
        }

        return true;
    }

    @Override
    public Pack createPack(String[] strings) {
        return new Pack(strings[1], Double.parseDouble(strings[0]));
    }

    @Override
    public List<Pack> createPacksFromFile(String path) {
        File file = new File(path);

        if (!file.exists()) {
            errorWrite("File not exists!");
        }

        List<Pack> packs = new ArrayList<>();

        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(file);

            List<String> lines = new BufferedReader(
                new InputStreamReader(inputStream, StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.toList());

            for (String line : lines) {

                String[] strings = delimitString(line);

                if (!validateStrings(strings)) {
                    errorWrite("Wrong import file!");
                    packs.clear();
                    break;
                }

                Pack pack = createPack(strings);
                packs.add(pack);
            }
            ;


        } catch (FileNotFoundException e) {
            packs.clear();
            log.error("File read error!");
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    log.error("Close file error!");
                }
            }
        }

        return packs;
    }

    @Override
    public String[] delimitString(String string) {
        return StringUtils.delimitedListToStringArray(string, DELIMITER);
    }

    private void errorWrite(String text) {
        System.err.println(text);
    }
}
