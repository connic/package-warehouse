package cz.konir.packagewarehouse.service.impl;

import cz.konir.packagewarehouse.database.Warehouse;
import cz.konir.packagewarehouse.service.WarehouseService;
import cz.konir.packagewarehouse.vo.Pack;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

@Service
@RequiredArgsConstructor
@Slf4j
public class WarehouseServiceImpl implements WarehouseService {

    private final Warehouse warehouse;

    @Override
    public void addPack(Pack pack) {
        if (ObjectUtils.isEmpty(pack.getPostNumber()) || pack.getWeight() == null) {
            log.warn("Wrong information package!");
            return;
        }

        warehouse.addPack(pack);
    }

    @Override
    public void addPacks(List<Pack> packs) {
        packs.forEach(this::addPack);
    }

    @Override
    public Map<String, Double> getWeightGroupedByPostCodes() {

        List<Pack> allPacks = warehouse.getAllPack();

        if (CollectionUtils.isEmpty(allPacks)) {
            return Collections.emptyMap();
        }

        Map<String, Double> result = allPacks.stream()
            .collect(Collectors.groupingBy(Pack::getPostNumber, Collectors.summingDouble(Pack::getWeight)));

        return result;
    }
}
