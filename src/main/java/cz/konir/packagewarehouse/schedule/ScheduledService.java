package cz.konir.packagewarehouse.schedule;

import cz.konir.packagewarehouse.service.WarehouseService;
import cz.konir.packagewarehouse.util.TextFormatUtil;
import java.util.Comparator;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ScheduledService {

    private final WarehouseService warehouseService;

    @Scheduled(cron = "0 * * * * *")
    public void printWeights() {
        Map<String, Double> weights = warehouseService.getWeightGroupedByPostCodes();

        weights.entrySet().stream()
            .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .forEach(entry -> {
            System.out.println(TextFormatUtil.weightOutputFormat(entry.getKey(), entry.getValue()));
        });
    }
}
