package cz.konir.packagewarehouse.util;

import java.util.Locale;

public class TextFormatUtil {

    public static String weightOutputFormat(String postCode, Double weight) {
        return String.format(Locale.ROOT, "%s %.3f", postCode, weight);
    }

}
