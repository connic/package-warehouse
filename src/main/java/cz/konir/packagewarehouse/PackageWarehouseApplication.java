package cz.konir.packagewarehouse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PackageWarehouseApplication {

    public static void main(String[] args) {
        SpringApplication.run(PackageWarehouseApplication.class, args);
    }

}
