package cz.konir.packagewarehouse.database;

import cz.konir.packagewarehouse.vo.Pack;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class Warehouse {

    private List<Pack> warehouse = new ArrayList<>();

    public void addPack(Pack pack) {
        warehouse.add(pack);
    }

    public List<Pack> getAllPack() {
        return warehouse;
    }

}
